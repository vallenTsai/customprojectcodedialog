sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/Input",
	"sap/m/Button",
	'sap/ui/model/Filter',
	'sap/m/MessageToast'
], function(Control, Input, Button, Filter, MessageToast) {
	"use strict";
	return Control.extend("newtype.sap.generic.CustomProjectCodeDialog", {
		metadata: {
			properties: {
				value: {
					type: "string",
					defaultValue: ""
				}
			},
			aggregations: {
				_input: {
					type: "sap.m.Input",
					multiple: false,
					visibility: "hidden"
				}
			}
		},
		init: function() {
			this.setAggregation("_input", new Input({
				value: this.getValue(),
				showValueHelp:true,
				valueHelpOnly:true,
				valueHelpRequest:this._onPress.bind(this)
			}));
		},
		setValue: function(sValue) {
			this.setProperty("value", sValue, true);
		},
		_onPress: function(oEvent) {
			var ctrl = this;

			$.ajax("/ErpApi/api/cmsnb")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);

					if (!ctrl._oDialog) {
						ctrl._oDialog = sap.ui.xmlfragment("newtype.sap.generic.view.CustomProjectCodeDialog", ctrl);
					}
					ctrl._oDialog.setModel(oData, "contorlProjectCode");

					// clear the old search filter
					ctrl._oDialog.getBinding("items").filter([]);

					ctrl._oDialog.open();
				});
		},
		handleClose: function(oEvent) {
			var ctrl = this;
			var aContexts = oEvent.getParameter("selectedContexts");

			if (aContexts && aContexts.length) {
				var Nb002 = aContexts.map(function(oContext) {
					return oContext.getObject().Nb002;
				}).join(", ");
				MessageToast.show("你選擇了:" + Nb002);

				ctrl.setValue(Nb002);
				ctrl.getAggregation("_input").setValue(Nb002);
			} else {
				MessageToast.show("未選擇任何資料.");
			}
		},
		handleSearch: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oBinding = oEvent.getSource().getBinding("items");
			var oFilterOR = new Filter([
				new Filter("Nb001", sap.ui.model.FilterOperator.Contains, sValue, false),
				new Filter("Nb002", sap.ui.model.FilterOperator.Contains, sValue, false)
			], false);

			oBinding.filter(oFilterOR);
		},
		renderer: function(oRM, oControl) {
			oRM.write("<div");
			oRM.writeControlData(oControl);
			oRM.addClass("genericOwnershipDepartmentDialog");
			oRM.writeClasses();
			oRM.write(">");
			oRM.renderControl(oControl.getAggregation("_input"));
			oRM.renderControl(oControl.getAggregation("_button"));
			oRM.write("</div>");
		}
	});
});